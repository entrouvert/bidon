import os

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'TEST': {
            'NAME': 'bidon-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63],
        },
    }
}
