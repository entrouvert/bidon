from django.db import models


class Variable(models.Model):
    name = models.CharField(max_length=100, verbose_name='name')
    settings = models.CharField(max_length=100)
